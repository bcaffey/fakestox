from random import randint

from django.contrib.auth.models import AbstractUser
from django.db.models import CharField, DateTimeField, DecimalField, ForeignKey, Model, Sum, CASCADE, Index


class User(AbstractUser):
    """
    We'll probably need to add fields later, so don't use Django's built-in User.

    I know, I know, YAGNI. Tough cookies.
    """

    class Meta:
        ordering = ['-date_joined']

    @property
    def cash(self):
        return self.transfers.aggregate(Sum('amount'))['amount__sum']

    @property
    def holdings_value(self):
        return sum(holding.value for holding in self.holdings)  # Aggregate unavailable due to live price fetching

    @property
    def portfolio_value(self):
        return self.cash + self.holdings_value

    @property
    def lifetime_return(self):
        raise NotImplementedError

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        """Start new users with $1,000,000."""
        new = not self.pk
        super().save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

        if new:
            Transfer.objects.create(amount=1_000_000, user=self)


class Symbol(Model):
    symbol = CharField(max_length=8)
    name = CharField(max_length=255, blank=True)

    class Meta:
        indexes = [Index(fields=['symbol'])]
        ordering = ['symbol']

    @property
    def price(self):
        # TODO: Connect to external API
        return 100 + randint(-10, 10)


class Trade(Model):
    symbol = ForeignKey(Symbol, on_delete=CASCADE)
    price = DecimalField(decimal_places=2, max_digits=12)
    user = ForeignKey(User, on_delete=CASCADE)
    ts = DateTimeField(auto_now_add=True)

    class Meta:
        indexes = [Index(fields=['ts'])]
        ordering = ['-ts']


class Transfer(Model):
    amount = DecimalField(decimal_places=2, max_digits=12)
    user = ForeignKey(User, on_delete=CASCADE)
    ts = DateTimeField(auto_now_add=True)

    class Meta:
        indexes = [Index(fields=['ts'])]
        ordering = ['-ts']
