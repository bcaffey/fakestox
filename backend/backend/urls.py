from django.contrib import admin
from django.urls import include, path
from rest_framework.routers import DefaultRouter

from fakestox.views import GroupViewSet, UserViewSet, SymbolViewSet, TradeViewSet, TransferViewSet

router = DefaultRouter()
router.register('users', UserViewSet)
router.register('groups', GroupViewSet)
router.register('symbols', SymbolViewSet)
router.register('trades', TradeViewSet)
router.register('transfers', TransferViewSet)

urlpatterns = [
    path('v1/', include(router.urls)),  # More like v0.1 :D
    path('admin/', admin.site.urls),  # Django Admin might be redundant with DRF browsable API
    path('api-auth/', include('rest_framework.urls')),
]
